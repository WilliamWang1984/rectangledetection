#!/usr/bin/env python

'''
Simple Rectangle detector program, based on
Simple "Square Detector" program.
Loads several images sequentially and tries to find squares (rectangles) in each image.

Implementation Courtesy of:
https://github.com/opencv/opencv/blob/master/samples/python/squares.py (for square/rectangle detection)
https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/ (for fast bounding box merging)
'''

# Python 2/3 compatibility
from __future__ import print_function
import sys
PY3 = sys.version_info[0] == 3

if PY3:
    xrange = range

import numpy as np
import cv2 as cv

def non_max_suppression_fast(boxes, overlapThresh):
   # if there are no boxes, return an empty list
   if len(boxes) == 0:
      return []

   # if the bounding boxes integers, convert them to floats --
   # this is important since we'll be doing a bunch of divisions
   if boxes.dtype.kind == "i":
      boxes = boxes.astype("float")

   # initialize the list of picked indexes   
   pick = []

   # grab the coordinates of the bounding boxes
   x1 = boxes[:,0]
   y1 = boxes[:,1]
   x2 = boxes[:,2]
   y2 = boxes[:,3]

   print("Printing x1, y1, x2, y2 in NMS:")
   print(x1, y1, x2, y2)

   # compute the area of the bounding boxes and sort the bounding
   # boxes by the bottom-right y-coordinate of the bounding box
   area = (x2 - x1 + 1) * (y2 - y1 + 1)
   idxs = np.argsort(y2)

   # keep looping while some indexes still remain in the indexes
   # list
   while len(idxs) > 0:
      # grab the last index in the indexes list and add the
      # index value to the list of picked indexes
      last = len(idxs) - 1
      i = idxs[last]
      pick.append(i)

      # find the largest (x, y) coordinates for the start of
      # the bounding box and the smallest (x, y) coordinates
      # for the end of the bounding box
      xx1 = np.maximum(x1[i], x1[idxs[:last]])
      yy1 = np.maximum(y1[i], y1[idxs[:last]])
      xx2 = np.minimum(x2[i], x2[idxs[:last]])
      yy2 = np.minimum(y2[i], y2[idxs[:last]])

      # compute the width and height of the bounding box
      w = np.maximum(0, xx2 - xx1 + 1)
      h = np.maximum(0, yy2 - yy1 + 1)

      # compute the ratio of overlap
      overlap = (w * h) / area[idxs[:last]]

      # delete all indexes from the index list that have
      idxs = np.delete(idxs, np.concatenate(([last],
         np.where(overlap > overlapThresh)[0])))

   # return only the bounding boxes that were picked using the
   # integer data type
   return boxes[pick].astype("int")

def angle_cos(p0, p1, p2):
    d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
    return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

def find_rectangles(img):
    img = cv.GaussianBlur(img, (5, 5), 0)
    rectangles = []
    for gray in cv.split(img):
        for thrs in xrange(0, 255, 26):
            if thrs == 0:
                bin = cv.Canny(gray, 0, 50, apertureSize=5)
                bin = cv.dilate(bin, None)
            else:
                _retval, bin = cv.threshold(gray, thrs, 255, cv.THRESH_BINARY)
            #bin, contours, _hierarchy = cv.findContours(bin, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
            contourResults = cv.findContours(bin, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
            # Opencv version compatibility
            if len(contourResults) <= 2:
                contours = contourResults[0]
            elif len(contourResults) > 2:
                contours = contourResults[1]

            for cnt in contours:
                cnt_len = cv.arcLength(cnt, True)
                cnt = cv.approxPolyDP(cnt, 0.02*cnt_len, True)
                if len(cnt) == 4 and cv.contourArea(cnt) > 1000 and cv.isContourConvex(cnt):
                    cnt = cnt.reshape(-1, 2)
                    max_cos = np.max([angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
                    if max_cos < 0.1:
                        rectangles.append(cnt)
    return rectangles

# reverse cv2.boundingRect([][][][])
def rectBounding(box):
    x, y, w, h = box
    topLeft = [x, y]
    bottomLeft = [x, h]
    bottomRight = [w, h]
    topRight = [w, y]
    return np.vstack((topLeft, bottomLeft, bottomRight, topRight))

def main():

    boxes = np.zeros((1000, 4), dtype=int)

    from glob import glob
    for fn in glob('./*.jpg'):
        img = cv.imread(fn)
        rects = find_rectangles(img)
        print("Printing rectangles:")
        print(rects)

        # Convert TopLeft, BottomLeft, BottomRight, TopRight - cnt bounding boxes to x, y, w, h BBs.
        mat_index = 0
        for rect in rects:
            x, y, w, h = cv.boundingRect(rect)
            #print(x, y, w, h)

            # Convert x, y, w, h BBs to x1, y1, x2, y2 BBs
            x1, y1, x2, y2 = x, y, x+w, y+h
            print("Printing x1, y1, x2, y2")
            print(x1, y1, x2, y2)

            # selecting only tiny boxes on swatch, ignore greater rectangles (i.e. whole image border or similar bigger rects) 
            if w*h < 10000:
                boxes[mat_index,:] = x1, y1, x2, y2
                mat_index += 1

        print("Printing boxes:")
        print(boxes)        

        reducedBoxes = non_max_suppression_fast(boxes, 0.8)
        print("Printing rectangles after NMS:")
        print(reducedBoxes)
        

        # delete [0,0,0,0]
        validBoxes = reducedBoxes[:-1, :] 
        print("Printing rectangles after remove [0,0,0,0]")
        print(validBoxes)

        convBoxes = []
        textInd = 1
        for validBox in validBoxes:
            convBox = rectBounding(validBox)
            convBoxes.append(convBox)
            cv.putText(img, str(textInd), (validBox[0], validBox[3]), cv.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), lineType=cv.LINE_AA)
            textInd += 1

        print("Converted boxes are:")
        print(convBoxes)

        cv.drawContours( img, convBoxes, -1, (0, 255, 255), 1 )
        
        cv.imshow('Boxes', img)
        ch = cv.waitKey()
        if ch == 27:
            break

    print('Done')


if __name__ == '__main__':
    print(__doc__)
    main()
cv.destroyAllWindows()
